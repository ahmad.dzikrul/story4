from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from .forms import RegisterForm
from django.contrib.auth import authenticate, login
from django.contrib import messages

# Create your views here.
def register(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request,user)
        else:
            messages.warning(request,"Registrasi anda gagal!!")
        return redirect("/home")
    else:
        form = RegisterForm()
    return render(request, "register/sign_up.html",{"form":form})