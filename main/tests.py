from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver
from .models import Person, anggota, daftar_kegiatan


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)


class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

class Teststory6(TestCase):
    def test_url_slash_ada(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_home_ada(self):
        response = self.client.get('/home/')
        self.assertEqual(response.status_code, 200)

    def test_url_about_me_ada(self):
        response = self.client.get('/about_me/')
        self.assertEqual(response.status_code, 200)

    def test_url_education_ada(self):
        response = self.client.get('/education/')
        self.assertEqual(response.status_code, 200)

    def test_url_skills_ada(self):
        response = self.client.get('/skills/')
        self.assertEqual(response.status_code, 200)

    def test_url_projects_ada(self):
        response = self.client.get('/projects/')
        self.assertEqual(response.status_code, 200)

    def test_url_achievement_ada(self):
        response = self.client.get('/achievement/')
        self.assertEqual(response.status_code, 200)

    def test_url_matkul_ada(self):
        response = self.client.get('/matkul/')
        self.assertEqual(response.status_code, 200)

    def test_url_mata_kuliah_ada(self):
        response = self.client.get('/mata_kuliah/')
        self.assertEqual(response.status_code, 302)

    def test_url_list_matkul_ada(self):
        response = self.client.get('/list_matkul/')
        self.assertEqual(response.status_code, 200)

    def test_navbar(self):
        response = self.client.get('/')
        page_respon = response.content.decode('utf8')
        self.assertIn("About me",page_respon)
        self.assertIn("Education",page_respon)
        self.assertIn("Skills",page_respon)
        self.assertIn("Projects",page_respon)
        self.assertIn("Achievement",page_respon)
        self.assertIn("form mata kuliah",page_respon)
        self.assertIn("AHMAD DZIKRUL FIKRI",page_respon)

    def test_url_pencarian_ada(self):
        response = self.client.get('/pencarian/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_datas_ada(self):
        response = self.client.get('/datas?q=frozen')
        self.assertEqual(response.status_code, 200)