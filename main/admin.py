from django.contrib import admin
from .models import Person, daftar_kegiatan, anggota

# Register your models here.
admin.site.register(Person)
admin.site.register(daftar_kegiatan)
admin.site.register(anggota)