from django.db import models

# Create your models here.
class Person(models.Model):
    mata_kuliah = models.CharField(max_length=1000)
    dosen = models.CharField(max_length=1000, null=True)
    jumlah_sks = models.CharField(max_length=1000,null=True)
    deskripsi = models.CharField(max_length=1000,null=True)
    semester_tahun = models.CharField(max_length=1000,null=True)

class anggota(models.Model):
    nama_anggota = models.CharField(max_length=1000,null=True)

class daftar_kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=1000,null=True)
    anggota_kelompok = models.ManyToManyField(anggota)
