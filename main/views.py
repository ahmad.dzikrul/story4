from django.shortcuts import render
from datetime import datetime, date
from .forms import Input_form, Input_form_2, Input_form_3
from django.http import HttpResponseRedirect
from .models import Person, daftar_kegiatan, anggota
from django.http import JsonResponse
import requests #Note: kalau mau pake ini, kita harus "pip install requests" dulu di cmd venv kita
import json


mhs_name = 'Ahmad Dzikrul Fikri' 
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999,7,29) 
npm = 1806196806 
angkatan = 2018
universitas = 'Universitas Indonesia'
hobi = 'Tidur sambil main HP'
deskripsi = 'Saya adalah pribadi yang senang mencoba hal baru. Saya berharap mendapatkan pengalaman yang nantinya akan berguna bagi kehidupan saya'
SD = "SDN Bojongrangkas 04"
SMP = "SMPN 1 Cibungbulang"
SMA = "SMAN 1 Leuwiliang"
sosmed1 = "ahmaddzikrulfikri"
sosmed2 = "Ahmad Dzikrul Fikri"
desc_form = "Silahkan isi data mata kuliah anda"
desc_form_kegiatan = "Silahkan isi data kegiatan anda"
aktivitas = "Mengerjakan tugas PPW"
organisasi = "Staff biro kewirausahaan HMGS, Kabiro kewirausahaan HMGS"
kepanitiaan = "Staff divisi tugas PDGs, Koor lomba 2 GGC UI"
prestasi = "Juara 2 SIC UI 2017, Juara turnamen basket SMK Cahaya 2016"

def home(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'angkatan': angkatan, 'universitas':universitas,'hobi':hobi, 'deskripsi': deskripsi, 'sosmed1':sosmed1, 'sosmed2':sosmed2,'SD':SD, 'SMP':SMP,'SMA':SMA}
    return render(request, 'main/home.html',response)

def accordion(request):
    response = {'aktivitas':aktivitas, 'organisasi':organisasi, "kepanitiaan":kepanitiaan, "prestasi":prestasi}
    return render(request, 'main/accordion.html',response)

def about_me(request):
    response = {'deskripsi': deskripsi}
    return render(request, 'main/about_me.html',response)

def matkul(request):
    response = {'deskripsi' : desc_form, 'input_form' : Input_form}
    return render(request, 'main/matkul.html',response)

def kegiatan(request):
    response = {'deskripsi' : desc_form_kegiatan, 'input_form' : Input_form_2}
    return render(request, 'main/kegiatan.html',response)

def education(request):
    return render(request, 'main/education.html')

def skills(request):
    return render(request, 'main/skills.html')

def projects(request):
    return render(request, 'main/projects.html')

def achievement(request):
    return render(request, 'main/achievement.html')

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0

def mata_kuliah(request):
    form = Input_form(request.POST or None)
    if (form.is_valid and request.method == 'POST') :
        form.save()
        return HttpResponseRedirect('/list_matkul')
    else:
        return HttpResponseRedirect('/')

def read_person(request):
    persons = Person.objects.all()
    response = {
        'persons':persons
    }
    return render(request, 'main/list_matkul.html', response)

def delete(request,nomor):
    Person.objects.filter(id=nomor).delete()
    return HttpResponseRedirect('/list_matkul')

def detail(request,nomor):
    details = Person.objects.get(id=nomor)
    response = {
        'details':details
    }
    return render(request, 'main/detail.html', response)

def pencarian(request):
    try :
        request.session['has_search'] = request.session['has_search'] +1
    except :
        request.session['has_search'] = 1
    response = {
        "jumlah_pencarian":request.session['has_search']
    }
    return render(request, 'main/pencarian.html',response)

def datas(request):
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + request.GET['q']
    ret = requests.get(url)
    data = json.loads(ret.content)
    return JsonResponse(data, safe = False)

