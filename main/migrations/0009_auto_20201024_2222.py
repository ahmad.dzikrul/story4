# Generated by Django 3.1.2 on 2020-10-24 15:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0008_daftar_kegiatan_anggota_kelompok'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='daftar_kegiatan',
            name='anggota_kelompok',
        ),
        migrations.CreateModel(
            name='post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('anggota_kelompok', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='main.daftar_kegiatan')),
            ],
        ),
    ]
