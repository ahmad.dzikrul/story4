# Generated by Django 3.1.2 on 2020-10-24 15:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_anggota'),
    ]

    operations = [
        migrations.AddField(
            model_name='daftar_kegiatan',
            name='anggota_kelompok',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='main.anggota'),
        ),
    ]
