from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('home/', views.home, name='home'),
    path('about_me/', views.about_me, name='about_me'),
    path('education/', views.education, name='education'),
    path('skills/', views.skills, name='skills'),
    path('projects/', views.projects, name='projects'),
    path('achievement/', views.achievement, name='achievement'),
    path('matkul/', views.matkul, name='matkul'),
    path('accordion/', views.accordion, name='accordion'),
    path('mata_kuliah/', views.mata_kuliah, name='mata_kuliah'),
    path('list_matkul/', views.read_person, name='read'),
    path('detail/<int:nomor>/', views.detail, name='detail'),
    path('delete/<int:nomor>/', views.delete, name='delete'),
    path('pencarian/', views.pencarian, name='pencarian'),
    path('datas', views.datas, name='datas'),
]
