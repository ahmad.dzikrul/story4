from django import forms
from .models import Person, daftar_kegiatan, anggota

class Input_form(forms.ModelForm):
    class Meta:
        model = Person
        fields = '__all__'

    error_messages = {
        'required' : 'Please Type'
    }

    input_attrs_matkul = {
        'type' : 'text',
        'placeholder' : 'Nama mata kuliah'
    }

    input_attrs_dosen ={
        'type' : 'text',
        'placeholder' : 'Nama Dosen'
    }

    input_attrs_sks ={
        'type' : 'text',
        'placeholder' : 'Jumlah SKS mata kuliah'
    }

    input_attrs_deskripsi ={
        'type' : 'text',
        'placeholder' : 'Deskripsi mata kuliah'
    }

    input_attrs_semester ={
        'type' : 'text',
        'placeholder' : 'Gasal/Genap - Tahun'
    }

    mata_kuliah = forms.CharField(label='Mata kuliah ', required=True, max_length=1000, widget=forms.TextInput(attrs=input_attrs_matkul))
    dosen = forms.CharField(label='Dosen ', required=True, max_length=1000, widget=forms.TextInput(attrs=input_attrs_dosen))
    jumlah_sks = forms.CharField(label='Jumlah SKS ', required=True, max_length=1000, widget=forms.TextInput(attrs=input_attrs_sks))
    deskripsi = forms.CharField(label='Deskripsi ', required=True, max_length=1000, widget=forms.TextInput(attrs=input_attrs_deskripsi))
    semester_tahun = forms.CharField(label='Semester ', required=True, max_length=1000, widget=forms.TextInput(attrs=input_attrs_semester))

class Input_form_2(forms.ModelForm):
    class Meta:
        model = daftar_kegiatan
        fields = '__all__'

    error_messages = {
        'required' : 'Please Type'
    }

    input_attrs_nama_kegiatan = {
        'type' : 'text',
        'placeholder' : 'Nama kegiatan'
    }

    nama_kegiatan = forms.CharField(label='Nama kegiatan ', required=True, max_length=1000, widget=forms.TextInput(attrs=input_attrs_nama_kegiatan))

class Input_form_3(forms.ModelForm):
    class Meta:
        model = anggota
        fields = '__all__'

    error_messages = {
        'required' : 'Please Type'
    }

    input_attrs_nama_anggota = {
        'type' : 'text',
        'placeholder' : 'Nama'
    }

    nama_anggota = forms.CharField(label='Nama anggota ', required=True, max_length=1000, widget=forms.TextInput(attrs=input_attrs_nama_anggota))
